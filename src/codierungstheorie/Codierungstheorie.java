/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codierungstheorie;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Henning
 */
public class Codierungstheorie {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int k;
        int q;
        try {
            //Input
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            do {
                System.out.print("k = ");
                k = Integer.parseInt(in.readLine());
                if (!isPrime(k)) {
                    System.out.println("Variable k must be a prime nummber!");
                }
            } while (!isPrime(k));
            do {
                System.out.print("q = ");
                q = Integer.parseInt(in.readLine());
                if (!isPrime(q)) {
                    System.out.println("Variable q must be a prime nummber!");
                }
            } while (!isPrime(q));

            //Matrix deklarieren
            int x = (int) (Math.pow(q, k) - 1) / (q - 1);
            int[][] A;
            A = new int[x][x];

            //Endlicher Körper bestimmen
            int[][] Fadd = new int[q][q];
            for (int i = 0; i < q; i++) {
                for (int j = 0; j < q; j++) {
                    Fadd[i][j] = (i + j) % q;
                }
            }

            int[][] Fmul = new int[q][q];
            for (int i = 0; i < q; i++) {
                for (int j = 0; j < q; j++) {
                    Fmul[i][j] = (i * j) % q;
                }
            }

            //Klassenbestimmen
            int[] z = new int[k];
            for (int i = 0; i < k; i++) {
                z[i] = 0;
            }
            z[k - 1] += 1;
            String t = "";

            //Generate all possible codes into V
            int[] V = new int[(int) (Math.pow(q, k) - 1)];
            String[] S = new String[(int) (Math.pow(q, k) - 1)];
            for (int i = 0; i < (int) (Math.pow(q, k) - 1); i++) {
                //Generate code as string
                for (int j = 0; j < k; j++) {
                    t += z[j];
                }

                //add +1 to code
                z[k - 1] += 1;
                for (int j = k - 1; j > 0 && z[j] == q; j--) {
                    z[j] = 0;
                    z[j - 1] += 1;
                }

                //convert codestring to int and reset codestring
                V[i] = Integer.parseInt(t);
                S[i] = t;
                t = "";
            }

            for (int i = 0; i < (int) (Math.pow(q, k) - 1); i++) {
                System.out.println(S[i]);
            }

            //Generate Klassenarray
            String[] r = new String[x];
            r[0] = S[0];
            r[1] = "10";
            r[2] = "11";
            r[3] = "12";

            for (int i = 0; i < k; i++) {

            }

            //Matrix berechnen
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < x; j++) {
                    int s = 0;
                    for (int e = 0; e < k; e++) {
                        System.out.println("r substring = " + r[i].substring(e, e+1));
                        System.out.println("r  = " + Integer.parseInt(r[i].substring(e, e+1)));
                        s = Fadd[s][ Fmul[Integer.parseInt(r[i].substring(e, e+1))][Integer.parseInt(r[j].substring(e, e+1))] ];
                    }
                    if(s==0) A[i][j] = 1;
                    else A[i][j] = 0;

                }
            }
            
            //Matrix Ausgeben
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < x; j++) {
                    System.out.print(A[i][j] + " ");

                }
                System.out.println();;
            }

        } catch (IOException ex) {
            Logger.getLogger(Codierungstheorie.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static boolean isPrime(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isMultiple(String n, String m) {
        double x = 0;
        for (int i = 0; i < n.length() && n.charAt(i) != '0' && m.charAt(i) != '0'; i++) {
            if (n.charAt(i) != '0' && m.charAt(i) != n.charAt(i)) {
                return false;
            }
            if (m.charAt(i) != '0' && m.charAt(i) != n.charAt(i)) {
                return false;
            }
            if (x == 0) {
                x = (double) m.charAt(i) / (double) n.charAt(i);
            } else {
                double y = (double) m.charAt(i) / (double) n.charAt(i);
                if (x != y) {
                    return false;
                }
            }
        }

        return true;
    }

    public static int skalarproduct(String n, String m) {

        return 0;
    }
}
